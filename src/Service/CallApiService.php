<?php


namespace App\Service;


use Symfony\Contracts\HttpClient\HttpClientInterface;

class CallApiService
{
    /**
     * @var HttpClientInterface
     */
    private HttpClientInterface $client;

    /**
     * CallApiService constructor.
     */
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function getFranceData(): array
    {
        return $this->getApi('FranceLiveGlobalData');
    }

    public function getAllData(): array
    {
        return $this->getApi('AllLiveData');
    }

    public function getDepartmentData(string $department): array
    {
        return $this->getApi('LiveDataByDepartement?Departement=' . $department);
    }

    public function getAllDataByDate(string $date): array
    {
        return $this->getApi('AllDataByDate?date=' . $date);
    }

    private function getApi(string $var): array
    {
        $response = $this->client->request(
            'GET',
            'https://coronavirusapi-france.vercel.app/' . $var
        );

        return $response->toArray();
    }
}